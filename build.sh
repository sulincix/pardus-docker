#!/bin/bash
if [ ! -f /usr/share/debootstrap/scripts/yirmibir ] ; then
    ln -s stable /usr/share/debootstrap/scripts/yirmibir
fi
# rootfs create
debootstrap --variant=minbase --arch=amd64 yirmibir rootfs https://depo.pardus.org.tr/pardus/
chroot rootfs apt update --allow-insecure-repositories
chroot rootfs apt install pardus-archive-keyring --allow-unauthenticated -y -o Dpkg::Options::="--force-confnew"
chroot rootfs apt full-upgrade -y -o Dpkg::Options::="--force-confnew"
# extra packages
pkgs=(
    man-db util-linux debianutils info coreutils bc bat binutils file
    openssh-client rsync mcrypt gzip tar cpio less diffutils whois
    grep sed findutils gawk procps pciutils inxi lshw dmidecode hwinfo
    ipmitool ncal calendar kmod passwd adduser tasksel at cowsay tree
    htop atop sysstat nmon apachetop iftop lsof psmisc traceroute
    fdisk parted xz-utils iproute2 iputils-ping curl wget
)
chroot rootfs apt install ${pkgs[@]} -y -o Dpkg::Options::="--force-confnew" --no-install-recommends
# clear rootfs
chroot rootfs apt clean
rm -rf rootfs/var/lib/apt/lists/*
find rootfs/var/log/ -type f -exec rm -f '{}' \;
# remove /root and recreate
rm -rf rootfs/root
mkdir -p rootfs/root
chmod 700 rootfs/root
chown root:root rootfs/root
# /bin/sh relink
rm rootfs/bin/sh
ln -s bash rootfs/bin/sh
# docker packaging
tar -cv -C rootfs . | docker import -
